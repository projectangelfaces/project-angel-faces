#!/usr/bin/php
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rchaplin
 * Date: 6/21/13
 * Time: 11:13 AM
 * To change this template use File | Settings | File Templates.
 */

$models = array(
    'addresses',
    'addresses_profiles',
    'addresstypes',
    'agendas',
    'agendatypes',
    'areas',
    'categories',
    'comments',
    'compost',
    'documents',
    'events',
    'expensetypes',
    'fees',
    'functiontypes',
    'incometypes',
    'inventories',
    'keywords',
    'keywords_pages',
    'keywords_posts',
    'ledgers',
    'locations',
    'pages',
    'phones',
    'phones_profiles',
    'posts',
    'posts_tags',
    'profiles',
    'profiletypes',
    'programs',
    'projects',
    'projecttypes',
    'schedules',
    'scheduletypes',
    'tags',
    'tasks',
    'tasktypes',
    'teams',
    'teamtypes',
    'yield_measures',
    'yields',
    'yieldtypes'
);

foreach ($models as $model) {
//    system('lib/Cake/Console/cake bake model ' .$model);
//    system('lib/Cake/Console/cake bake controller --public --admin ' .$model);
    system('lib/Cake/Console/cake bake view ' . $model);
}