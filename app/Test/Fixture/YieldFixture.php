<?php
/**
 * YieldFixture
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Test.Fixtures
 * @since         PAF v 1.0
 *
 *
 */
class YieldFixture extends CakeTestFixture
{

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'location_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'yieldtype_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'yield_measure_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'index'),
		'yield' => array('type' => 'float', 'null' => false, 'default' => null),
		'year' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 6),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'fk_yields_locations1_idx' => array('column' => 'location_id', 'unique' => 0),
			'fk_yields_yield_types1_idx' => array('column' => 'yieldtype_id', 'unique' => 0),
			'fk_yields_yield_measure1_idx' => array('column' => 'yield_measure_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'location_id' => 1,
			'yieldtype_id' => 1,
			'yield_measure_id' => 1,
			'yield' => 1,
			'year' => 1,
			'created' => '2013-06-29 15:47:39',
			'modified' => '2013-06-29 15:47:39'
		),
	);

}
