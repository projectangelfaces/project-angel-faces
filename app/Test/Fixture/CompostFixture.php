<?php
/**
 * CompostFixture
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Test.Fixtures
 * @since         PAF v 1.0
 *
 *
 */
class CompostFixture extends CakeTestFixture
{

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'profile_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'location_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'yieldtype_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'cubic_ft' => array('type' => 'integer', 'null' => true, 'default' => null),
		'dt_received' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'dt_released' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'price' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '12,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'profile_id' => 1,
			'location_id' => 1,
			'yieldtype_id' => 1,
			'cubic_ft' => 1,
			'dt_received' => '2013-06-29 15:46:42',
			'dt_released' => '2013-06-29 15:46:42',
			'price' => 1,
			'created' => '2013-06-29 15:46:42',
			'modified' => '2013-06-29 15:46:42'
		),
	);

}
