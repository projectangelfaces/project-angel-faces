<?php
App::uses('Expensetype', 'Model');

/**
 * Expensetype Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class ExpensetypeTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.expensetype',
		'app.ledger',
		'app.incometype',
		'app.program',
		'app.document',
		'app.location',
		'app.project',
		'app.projecttype',
		'app.event',
		'app.agenda',
		'app.schedule',
		'app.functiontype',
		'app.events_profile',
		'app.profile',
		'app.profiletype',
		'app.area',
		'app.address',
		'app.addresstype',
		'app.addresses_profile',
		'app.compost',
		'app.yieldtype',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.task',
		'app.tasktype',
		'app.phone',
		'app.phones_profile',
		'app.scheduletype',
		'app.fee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Expensetype = ClassRegistry::init('Expensetype');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Expensetype);

		parent::tearDown();
	}

}
