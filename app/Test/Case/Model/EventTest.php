<?php
App::uses('Event', 'Model');

/**
 * Event Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Model

 * @since         PAF v 1.0
 *
 *
 */
class EventTest extends CakeTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.event',
		'app.location',
		'app.project',
		'app.program',
		'app.document',
		'app.profile',
		'app.profiletype',
		'app.area',
		'app.compost',
		'app.yieldtype',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.task',
		'app.tasktype',
		'app.address',
		'app.addresstype',
		'app.areas',
		'app.addresses_profile',
		'app.events_profile',
		'app.phone',
		'app.phones_profile',
		'app.projecttype',
		'app.fee'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp()
	{
		parent::setUp();
		$this->Event = ClassRegistry::init('Event');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown()
	{
		unset($this->Event);

		parent::tearDown();
	}

}
