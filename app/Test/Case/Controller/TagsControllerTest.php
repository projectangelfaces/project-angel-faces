<?php
App::uses('TagsController', 'Controller');

/**
 * TagsController Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Controller

 * @since         PAF v 1.0
 *
 *
 */
class TagsControllerTest extends ControllerTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tag',
		'app.post',
		'app.comment',
		'app.user',
		'app.group',
		'app.keyword',
		'app.page',
		'app.keywords_page',
		'app.keywords_post',
		'app.posts_tag'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex()
	{
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView()
	{
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd()
	{
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit()
	{
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete()
	{
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex()
	{
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView()
	{
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd()
	{
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit()
	{
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete()
	{
	}

}
