<?php
App::uses('AgendasController', 'Controller');

/**
 * AgendasController Test Case
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces

 * @package       App.Test.Controller

 * @since         PAF v 1.0
 *
 *
 */
class AgendasControllerTest extends ControllerTestCase
{

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.agenda',
		'app.agendatype',
		'app.event',
		'app.location',
		'app.project',
		'app.program',
		'app.document',
		'app.profile',
		'app.profiletype',
		'app.compost',
		'app.yieldtype',
		'app.inventory',
		'app.yield_measure',
		'app.yield',
		'app.ledger',
		'app.incometype',
		'app.expensetype',
		'app.schedule',
		'app.functiontype',
		'app.scheduletype',
		'app.task',
		'app.tasktype',
		'app.address',
		'app.addresstype',
		'app.area',
		'app.addresses_profile',
		'app.phone',
		'app.phones_profile',
		'app.profiles_program',
		'app.team',
		'app.profiles_team',
		'app.projecttype',
		'app.fee'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex()
	{
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView()
	{
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd()
	{
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit()
	{
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete()
	{
	}

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex()
	{
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView()
	{
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd()
	{
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit()
	{
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete()
	{
	}

}
