<?php
App::uses('AppController', 'Controller');
/**
 * Projects Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Project $Project
 */
class ProjectsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Project->recursive = 0;
		$this->set('projects', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Project->id = $id;
		if (!$this->Project->exists())
		{
			throw new NotFoundException(__('Invalid project'));
		}
		$this->set('project', $this->Project->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Project->create();
			if ($this->Project->save($this->request->data))
			{
				$this->Session->setFlash(__('The project has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		}
		$programs = $this->Project->Program->find('list');
		$projecttypes = $this->Project->Projecttype->find('list');
		$this->set(compact('programs', 'projecttypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Project->id = $id;
		if (!$this->Project->exists())
		{
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Project->save($this->request->data))
			{
				$this->Session->setFlash(__('The project has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Project->read(null, $id);
		}
		$programs = $this->Project->Program->find('list');
		$projecttypes = $this->Project->Projecttype->find('list');
		$this->set(compact('programs', 'projecttypes'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Project->id = $id;
		if (!$this->Project->exists())
		{
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->Project->delete())
		{
			$this->Session->setFlash(__('Project deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Project was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Project->recursive = 0;
		$this->set('projects', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Project->id = $id;
		if (!$this->Project->exists())
		{
			throw new NotFoundException(__('Invalid project'));
		}
		$this->set('project', $this->Project->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Project->create();
			if ($this->Project->save($this->request->data))
			{
				$this->Session->setFlash(__('The project has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		}
		$programs = $this->Project->Program->find('list');
		$projecttypes = $this->Project->Projecttype->find('list');
		$this->set(compact('programs', 'projecttypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Project->id = $id;
		if (!$this->Project->exists())
		{
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Project->save($this->request->data))
			{
				$this->Session->setFlash(__('The project has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Project->read(null, $id);
		}
		$programs = $this->Project->Program->find('list');
		$projecttypes = $this->Project->Projecttype->find('list');
		$this->set(compact('programs', 'projecttypes'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Project->id = $id;
		if (!$this->Project->exists())
		{
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->Project->delete())
		{
			$this->Session->setFlash(__('Project deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Project was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
