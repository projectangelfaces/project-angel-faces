<?php
App::uses('AppController', 'Controller');
/**
 * Phones Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Phone $Phone
 */
class PhonesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Phone->recursive = 0;
		$this->set('phones', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Phone->id = $id;
		if (!$this->Phone->exists())
		{
			throw new NotFoundException(__('Invalid phone'));
		}
		$this->set('phone', $this->Phone->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Phone->create();
			if ($this->Phone->save($this->request->data))
			{
				$this->Session->setFlash(__('The phone has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phone could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Phone->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Phone->id = $id;
		if (!$this->Phone->exists())
		{
			throw new NotFoundException(__('Invalid phone'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Phone->save($this->request->data))
			{
				$this->Session->setFlash(__('The phone has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phone could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Phone->read(null, $id);
		}
		$profiles = $this->Phone->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Phone->id = $id;
		if (!$this->Phone->exists())
		{
			throw new NotFoundException(__('Invalid phone'));
		}
		if ($this->Phone->delete())
		{
			$this->Session->setFlash(__('Phone deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phone was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Phone->recursive = 0;
		$this->set('phones', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Phone->id = $id;
		if (!$this->Phone->exists())
		{
			throw new NotFoundException(__('Invalid phone'));
		}
		$this->set('phone', $this->Phone->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Phone->create();
			if ($this->Phone->save($this->request->data))
			{
				$this->Session->setFlash(__('The phone has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phone could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Phone->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Phone->id = $id;
		if (!$this->Phone->exists())
		{
			throw new NotFoundException(__('Invalid phone'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Phone->save($this->request->data))
			{
				$this->Session->setFlash(__('The phone has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The phone could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Phone->read(null, $id);
		}
		$profiles = $this->Phone->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Phone->id = $id;
		if (!$this->Phone->exists())
		{
			throw new NotFoundException(__('Invalid phone'));
		}
		if ($this->Phone->delete())
		{
			$this->Session->setFlash(__('Phone deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phone was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
