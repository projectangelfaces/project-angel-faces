<?php
App::uses('AppController', 'Controller');
/**
 * Addresses Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Address $Address
 */
class AddressesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Address->recursive = 0;
		$this->set('addresses', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Address->id = $id;
		if (!$this->Address->exists())
		{
			throw new NotFoundException(__('Invalid address'));
		}
		$this->set('address', $this->Address->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Address->create();
			if ($this->Address->save($this->request->data))
			{
				$this->Session->setFlash(__('The address has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The address could not be saved. Please, try again.'));
			}
		}
		$addresstypes = $this->Address->Addresstype->find('list');
		$areas = $this->Address->Area->find('list');
		$profiles = $this->Address->Profile->find('list');
		$this->set(compact('addresstypes', 'areas', 'profiles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Address->id = $id;
		if (!$this->Address->exists())
		{
			throw new NotFoundException(__('Invalid address'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Address->save($this->request->data))
			{
				$this->Session->setFlash(__('The address has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The address could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Address->read(null, $id);
		}
		$addresstypes = $this->Address->Addresstype->find('list');
		$areas = $this->Address->Area->find('list');
		$profiles = $this->Address->Profile->find('list');
		$this->set(compact('addresstypes', 'areas', 'profiles'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Address->id = $id;
		if (!$this->Address->exists())
		{
			throw new NotFoundException(__('Invalid address'));
		}
		if ($this->Address->delete())
		{
			$this->Session->setFlash(__('Address deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Address was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Address->recursive = 0;
		$this->set('addresses', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Address->id = $id;
		if (!$this->Address->exists())
		{
			throw new NotFoundException(__('Invalid address'));
		}
		$this->set('address', $this->Address->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Address->create();
			if ($this->Address->save($this->request->data))
			{
				$this->Session->setFlash(__('The address has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The address could not be saved. Please, try again.'));
			}
		}
		$addresstypes = $this->Address->Addresstype->find('list');
		$areas = $this->Address->Area->find('list');
		$profiles = $this->Address->Profile->find('list');
		$this->set(compact('addresstypes', 'areas', 'profiles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Address->id = $id;
		if (!$this->Address->exists())
		{
			throw new NotFoundException(__('Invalid address'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Address->save($this->request->data))
			{
				$this->Session->setFlash(__('The address has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The address could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Address->read(null, $id);
		}
		$addresstypes = $this->Address->Addresstype->find('list');
		$areas = $this->Address->Area->find('list');
		$profiles = $this->Address->Profile->find('list');
		$this->set(compact('addresstypes', 'areas', 'profiles'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Address->id = $id;
		if (!$this->Address->exists())
		{
			throw new NotFoundException(__('Invalid address'));
		}
		if ($this->Address->delete())
		{
			$this->Session->setFlash(__('Address deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Address was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
