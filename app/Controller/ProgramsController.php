<?php
App::uses('AppController', 'Controller');
/**
 * Programs Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Program $Program
 */
class ProgramsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Program->recursive = 0;
		$this->set('programs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Program->id = $id;
		if (!$this->Program->exists())
		{
			throw new NotFoundException(__('Invalid program'));
		}
		$this->set('program', $this->Program->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Program->create();
			if ($this->Program->save($this->request->data))
			{
				$this->Session->setFlash(__('The program has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The program could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Program->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Program->id = $id;
		if (!$this->Program->exists())
		{
			throw new NotFoundException(__('Invalid program'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Program->save($this->request->data))
			{
				$this->Session->setFlash(__('The program has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The program could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Program->read(null, $id);
		}
		$profiles = $this->Program->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Program->id = $id;
		if (!$this->Program->exists())
		{
			throw new NotFoundException(__('Invalid program'));
		}
		if ($this->Program->delete())
		{
			$this->Session->setFlash(__('Program deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Program was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Program->recursive = 0;
		$this->set('programs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Program->id = $id;
		if (!$this->Program->exists())
		{
			throw new NotFoundException(__('Invalid program'));
		}
		$this->set('program', $this->Program->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Program->create();
			if ($this->Program->save($this->request->data))
			{
				$this->Session->setFlash(__('The program has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The program could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Program->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Program->id = $id;
		if (!$this->Program->exists())
		{
			throw new NotFoundException(__('Invalid program'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Program->save($this->request->data))
			{
				$this->Session->setFlash(__('The program has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The program could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Program->read(null, $id);
		}
		$profiles = $this->Program->Profile->find('list');
		$this->set(compact('profiles'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Program->id = $id;
		if (!$this->Program->exists())
		{
			throw new NotFoundException(__('Invalid program'));
		}
		if ($this->Program->delete())
		{
			$this->Session->setFlash(__('Program deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Program was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
