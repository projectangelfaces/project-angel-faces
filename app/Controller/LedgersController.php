<?php
App::uses('AppController', 'Controller');
/**
 * Ledgers Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Ledger $Ledger
 */
class LedgersController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Ledger->recursive = 0;
		$this->set('ledgers', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Ledger->id = $id;
		if (!$this->Ledger->exists())
		{
			throw new NotFoundException(__('Invalid ledger'));
		}
		$this->set('ledger', $this->Ledger->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Ledger->create();
			if ($this->Ledger->save($this->request->data))
			{
				$this->Session->setFlash(__('The ledger has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The ledger could not be saved. Please, try again.'));
			}
		}
		$incometypes = $this->Ledger->Incometype->find('list');
		$expensetypes = $this->Ledger->Expensetype->find('list');
		$events = $this->Ledger->Event->find('list');
		$programs = $this->Ledger->Program->find('list');
		$projects = $this->Ledger->Project->find('list');
		$locations = $this->Ledger->Location->find('list');
		$profiles = $this->Ledger->Profile->find('list');
		$this->set(compact('incometypes', 'expensetypes', 'events', 'programs', 'projects', 'locations', 'profiles'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Ledger->id = $id;
		if (!$this->Ledger->exists())
		{
			throw new NotFoundException(__('Invalid ledger'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Ledger->save($this->request->data))
			{
				$this->Session->setFlash(__('The ledger has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The ledger could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Ledger->read(null, $id);
		}
		$incometypes = $this->Ledger->Incometype->find('list');
		$expensetypes = $this->Ledger->Expensetype->find('list');
		$events = $this->Ledger->Event->find('list');
		$programs = $this->Ledger->Program->find('list');
		$projects = $this->Ledger->Project->find('list');
		$locations = $this->Ledger->Location->find('list');
		$profiles = $this->Ledger->Profile->find('list');
		$this->set(compact('incometypes', 'expensetypes', 'events', 'programs', 'projects', 'locations', 'profiles'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Ledger->id = $id;
		if (!$this->Ledger->exists())
		{
			throw new NotFoundException(__('Invalid ledger'));
		}
		if ($this->Ledger->delete())
		{
			$this->Session->setFlash(__('Ledger deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ledger was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Ledger->recursive = 0;
		$this->set('ledgers', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Ledger->id = $id;
		if (!$this->Ledger->exists())
		{
			throw new NotFoundException(__('Invalid ledger'));
		}
		$this->set('ledger', $this->Ledger->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Ledger->create();
			if ($this->Ledger->save($this->request->data))
			{
				$this->Session->setFlash(__('The ledger has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The ledger could not be saved. Please, try again.'));
			}
		}
		$incometypes = $this->Ledger->Incometype->find('list');
		$expensetypes = $this->Ledger->Expensetype->find('list');
		$events = $this->Ledger->Event->find('list');
		$programs = $this->Ledger->Program->find('list');
		$projects = $this->Ledger->Project->find('list');
		$locations = $this->Ledger->Location->find('list');
		$profiles = $this->Ledger->Profile->find('list');
		$this->set(compact('incometypes', 'expensetypes', 'events', 'programs', 'projects', 'locations', 'profiles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Ledger->id = $id;
		if (!$this->Ledger->exists())
		{
			throw new NotFoundException(__('Invalid ledger'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Ledger->save($this->request->data))
			{
				$this->Session->setFlash(__('The ledger has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The ledger could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Ledger->read(null, $id);
		}
		$incometypes = $this->Ledger->Incometype->find('list');
		$expensetypes = $this->Ledger->Expensetype->find('list');
		$events = $this->Ledger->Event->find('list');
		$programs = $this->Ledger->Program->find('list');
		$projects = $this->Ledger->Project->find('list');
		$locations = $this->Ledger->Location->find('list');
		$profiles = $this->Ledger->Profile->find('list');
		$this->set(compact('incometypes', 'expensetypes', 'events', 'programs', 'projects', 'locations', 'profiles'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Ledger->id = $id;
		if (!$this->Ledger->exists())
		{
			throw new NotFoundException(__('Invalid ledger'));
		}
		if ($this->Ledger->delete())
		{
			$this->Session->setFlash(__('Ledger deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ledger was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
