<?php
App::uses('AppController', 'Controller');
/**
 * Composts Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Compost $Compost
 */
class CompostsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Compost->recursive = 0;
		$this->set('composts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Compost->id = $id;
		if (!$this->Compost->exists())
		{
			throw new NotFoundException(__('Invalid compost'));
		}
		$this->set('compost', $this->Compost->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Compost->create();
			if ($this->Compost->save($this->request->data))
			{
				$this->Session->setFlash(__('The compost has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The compost could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Compost->Profile->find('list');
		$locations = $this->Compost->Location->find('list');
		$yieldtypes = $this->Compost->Yieldtype->find('list');
		$this->set(compact('profiles', 'locations', 'yieldtypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Compost->id = $id;
		if (!$this->Compost->exists())
		{
			throw new NotFoundException(__('Invalid compost'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Compost->save($this->request->data))
			{
				$this->Session->setFlash(__('The compost has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The compost could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Compost->read(null, $id);
		}
		$profiles = $this->Compost->Profile->find('list');
		$locations = $this->Compost->Location->find('list');
		$yieldtypes = $this->Compost->Yieldtype->find('list');
		$this->set(compact('profiles', 'locations', 'yieldtypes'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Compost->id = $id;
		if (!$this->Compost->exists())
		{
			throw new NotFoundException(__('Invalid compost'));
		}
		if ($this->Compost->delete())
		{
			$this->Session->setFlash(__('Compost deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Compost was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Compost->recursive = 0;
		$this->set('composts', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Compost->id = $id;
		if (!$this->Compost->exists())
		{
			throw new NotFoundException(__('Invalid compost'));
		}
		$this->set('compost', $this->Compost->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Compost->create();
			if ($this->Compost->save($this->request->data))
			{
				$this->Session->setFlash(__('The compost has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The compost could not be saved. Please, try again.'));
			}
		}
		$profiles = $this->Compost->Profile->find('list');
		$locations = $this->Compost->Location->find('list');
		$yieldtypes = $this->Compost->Yieldtype->find('list');
		$this->set(compact('profiles', 'locations', 'yieldtypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Compost->id = $id;
		if (!$this->Compost->exists())
		{
			throw new NotFoundException(__('Invalid compost'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Compost->save($this->request->data))
			{
				$this->Session->setFlash(__('The compost has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The compost could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Compost->read(null, $id);
		}
		$profiles = $this->Compost->Profile->find('list');
		$locations = $this->Compost->Location->find('list');
		$yieldtypes = $this->Compost->Yieldtype->find('list');
		$this->set(compact('profiles', 'locations', 'yieldtypes'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Compost->id = $id;
		if (!$this->Compost->exists())
		{
			throw new NotFoundException(__('Invalid compost'));
		}
		if ($this->Compost->delete())
		{
			$this->Session->setFlash(__('Compost deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Compost was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
