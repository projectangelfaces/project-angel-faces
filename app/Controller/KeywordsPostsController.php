<?php
App::uses('AppController', 'Controller');
/**
 * KeywordsPosts Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property KeywordsPost $KeywordsPost
 */
class KeywordsPostsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->KeywordsPost->recursive = 0;
		$this->set('keywordsPosts', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->KeywordsPost->id = $id;
		if (!$this->KeywordsPost->exists())
		{
			throw new NotFoundException(__('Invalid keywords post'));
		}
		$this->set('keywordsPost', $this->KeywordsPost->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->KeywordsPost->create();
			if ($this->KeywordsPost->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords post could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->KeywordsPost->Keyword->find('list');
		$posts = $this->KeywordsPost->Post->find('list');
		$this->set(compact('keywords', 'posts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->KeywordsPost->id = $id;
		if (!$this->KeywordsPost->exists())
		{
			throw new NotFoundException(__('Invalid keywords post'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->KeywordsPost->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords post could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->KeywordsPost->read(null, $id);
		}
		$keywords = $this->KeywordsPost->Keyword->find('list');
		$posts = $this->KeywordsPost->Post->find('list');
		$this->set(compact('keywords', 'posts'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->KeywordsPost->id = $id;
		if (!$this->KeywordsPost->exists())
		{
			throw new NotFoundException(__('Invalid keywords post'));
		}
		if ($this->KeywordsPost->delete())
		{
			$this->Session->setFlash(__('Keywords post deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Keywords post was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->KeywordsPost->recursive = 0;
		$this->set('keywordsPosts', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->KeywordsPost->id = $id;
		if (!$this->KeywordsPost->exists())
		{
			throw new NotFoundException(__('Invalid keywords post'));
		}
		$this->set('keywordsPost', $this->KeywordsPost->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->KeywordsPost->create();
			if ($this->KeywordsPost->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords post could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->KeywordsPost->Keyword->find('list');
		$posts = $this->KeywordsPost->Post->find('list');
		$this->set(compact('keywords', 'posts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->KeywordsPost->id = $id;
		if (!$this->KeywordsPost->exists())
		{
			throw new NotFoundException(__('Invalid keywords post'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->KeywordsPost->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords post has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords post could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->KeywordsPost->read(null, $id);
		}
		$keywords = $this->KeywordsPost->Keyword->find('list');
		$posts = $this->KeywordsPost->Post->find('list');
		$this->set(compact('keywords', 'posts'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->KeywordsPost->id = $id;
		if (!$this->KeywordsPost->exists())
		{
			throw new NotFoundException(__('Invalid keywords post'));
		}
		if ($this->KeywordsPost->delete())
		{
			$this->Session->setFlash(__('Keywords post deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Keywords post was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
