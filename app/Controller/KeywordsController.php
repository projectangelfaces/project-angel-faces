<?php
App::uses('AppController', 'Controller');
/**
 * Keywords Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Keyword $Keyword
 */
class KeywordsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Keyword->recursive = 0;
		$this->set('keywords', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Keyword->id = $id;
		if (!$this->Keyword->exists())
		{
			throw new NotFoundException(__('Invalid keyword'));
		}
		$this->set('keyword', $this->Keyword->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Keyword->create();
			if ($this->Keyword->save($this->request->data))
			{
				$this->Session->setFlash(__('The keyword has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keyword could not be saved. Please, try again.'));
			}
		}
		$pages = $this->Keyword->Page->find('list');
		$posts = $this->Keyword->Post->find('list');
		$this->set(compact('pages', 'posts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Keyword->id = $id;
		if (!$this->Keyword->exists())
		{
			throw new NotFoundException(__('Invalid keyword'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Keyword->save($this->request->data))
			{
				$this->Session->setFlash(__('The keyword has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keyword could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Keyword->read(null, $id);
		}
		$pages = $this->Keyword->Page->find('list');
		$posts = $this->Keyword->Post->find('list');
		$this->set(compact('pages', 'posts'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Keyword->id = $id;
		if (!$this->Keyword->exists())
		{
			throw new NotFoundException(__('Invalid keyword'));
		}
		if ($this->Keyword->delete())
		{
			$this->Session->setFlash(__('Keyword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Keyword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Keyword->recursive = 0;
		$this->set('keywords', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Keyword->id = $id;
		if (!$this->Keyword->exists())
		{
			throw new NotFoundException(__('Invalid keyword'));
		}
		$this->set('keyword', $this->Keyword->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Keyword->create();
			if ($this->Keyword->save($this->request->data))
			{
				$this->Session->setFlash(__('The keyword has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keyword could not be saved. Please, try again.'));
			}
		}
		$pages = $this->Keyword->Page->find('list');
		$posts = $this->Keyword->Post->find('list');
		$this->set(compact('pages', 'posts'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Keyword->id = $id;
		if (!$this->Keyword->exists())
		{
			throw new NotFoundException(__('Invalid keyword'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Keyword->save($this->request->data))
			{
				$this->Session->setFlash(__('The keyword has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keyword could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Keyword->read(null, $id);
		}
		$pages = $this->Keyword->Page->find('list');
		$posts = $this->Keyword->Post->find('list');
		$this->set(compact('pages', 'posts'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Keyword->id = $id;
		if (!$this->Keyword->exists())
		{
			throw new NotFoundException(__('Invalid keyword'));
		}
		if ($this->Keyword->delete())
		{
			$this->Session->setFlash(__('Keyword deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Keyword was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
