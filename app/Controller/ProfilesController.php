<?php
App::uses('AppController', 'Controller');
/**
 * Profiles Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Profile $Profile
 */
class ProfilesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Profile->recursive = 0;
		$this->set('profiles', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Profile->id = $id;
		if (!$this->Profile->exists())
		{
			throw new NotFoundException(__('Invalid profile'));
		}
		$this->set('profile', $this->Profile->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Profile->create();
			if ($this->Profile->save($this->request->data))
			{
				$this->Session->setFlash(__('The profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		}
		$profiletypes = $this->Profile->Profiletype->find('list');
		$parentProfiles = $this->Profile->ParentProfile->find('list');
		$addresses = $this->Profile->Address->find('list');
		$phones = $this->Profile->Phone->find('list');
		$programs = $this->Profile->Program->find('list');
		$teams = $this->Profile->Team->find('list');
		$this->set(compact('profiletypes', 'parentProfiles', 'addresses', 'phones', 'programs', 'teams'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Profile->id = $id;
		if (!$this->Profile->exists())
		{
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Profile->save($this->request->data))
			{
				$this->Session->setFlash(__('The profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Profile->read(null, $id);
		}
		$profiletypes = $this->Profile->Profiletype->find('list');
		$parentProfiles = $this->Profile->ParentProfile->find('list');
		$addresses = $this->Profile->Address->find('list');
		$phones = $this->Profile->Phone->find('list');
		$programs = $this->Profile->Program->find('list');
		$teams = $this->Profile->Team->find('list');
		$this->set(compact('profiletypes', 'parentProfiles', 'addresses', 'phones', 'programs', 'teams'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Profile->id = $id;
		if (!$this->Profile->exists())
		{
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->Profile->delete())
		{
			$this->Session->setFlash(__('Profile deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Profile was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Profile->recursive = 0;
		$this->set('profiles', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Profile->id = $id;
		if (!$this->Profile->exists())
		{
			throw new NotFoundException(__('Invalid profile'));
		}
		$this->set('profile', $this->Profile->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Profile->create();
			if ($this->Profile->save($this->request->data))
			{
				$this->Session->setFlash(__('The profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		}
		$profiletypes = $this->Profile->Profiletype->find('list');
		$parentProfiles = $this->Profile->ParentProfile->find('list');
		$addresses = $this->Profile->Address->find('list');
		$phones = $this->Profile->Phone->find('list');
		$programs = $this->Profile->Program->find('list');
		$teams = $this->Profile->Team->find('list');
		$this->set(compact('profiletypes', 'parentProfiles', 'addresses', 'phones', 'programs', 'teams'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Profile->id = $id;
		if (!$this->Profile->exists())
		{
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Profile->save($this->request->data))
			{
				$this->Session->setFlash(__('The profile has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The profile could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Profile->read(null, $id);
		}
		$profiletypes = $this->Profile->Profiletype->find('list');
		$parentProfiles = $this->Profile->ParentProfile->find('list');
		$addresses = $this->Profile->Address->find('list');
		$phones = $this->Profile->Phone->find('list');
		$programs = $this->Profile->Program->find('list');
		$teams = $this->Profile->Team->find('list');
		$this->set(compact('profiletypes', 'parentProfiles', 'addresses', 'phones', 'programs', 'teams'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Profile->id = $id;
		if (!$this->Profile->exists())
		{
			throw new NotFoundException(__('Invalid profile'));
		}
		if ($this->Profile->delete())
		{
			$this->Session->setFlash(__('Profile deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Profile was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
