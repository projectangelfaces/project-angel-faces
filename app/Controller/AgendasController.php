<?php
App::uses('AppController', 'Controller');
/**
 * Agendas Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Agenda $Agenda
 */
class AgendasController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Agenda->recursive = 0;
		$this->set('agendas', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Agenda->id = $id;
		if (!$this->Agenda->exists())
		{
			throw new NotFoundException(__('Invalid agenda'));
		}
		$this->set('agenda', $this->Agenda->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Agenda->create();
			if ($this->Agenda->save($this->request->data))
			{
				$this->Session->setFlash(__('The agenda has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The agenda could not be saved. Please, try again.'));
			}
		}
		$agendatypes = $this->Agenda->Agendatype->find('list');
		$events = $this->Agenda->Event->find('list');
		$this->set(compact('agendatypes', 'events'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Agenda->id = $id;
		if (!$this->Agenda->exists())
		{
			throw new NotFoundException(__('Invalid agenda'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Agenda->save($this->request->data))
			{
				$this->Session->setFlash(__('The agenda has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The agenda could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Agenda->read(null, $id);
		}
		$agendatypes = $this->Agenda->Agendatype->find('list');
		$events = $this->Agenda->Event->find('list');
		$this->set(compact('agendatypes', 'events'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Agenda->id = $id;
		if (!$this->Agenda->exists())
		{
			throw new NotFoundException(__('Invalid agenda'));
		}
		if ($this->Agenda->delete())
		{
			$this->Session->setFlash(__('Agenda deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Agenda was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Agenda->recursive = 0;
		$this->set('agendas', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Agenda->id = $id;
		if (!$this->Agenda->exists())
		{
			throw new NotFoundException(__('Invalid agenda'));
		}
		$this->set('agenda', $this->Agenda->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Agenda->create();
			if ($this->Agenda->save($this->request->data))
			{
				$this->Session->setFlash(__('The agenda has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The agenda could not be saved. Please, try again.'));
			}
		}
		$agendatypes = $this->Agenda->Agendatype->find('list');
		$events = $this->Agenda->Event->find('list');
		$this->set(compact('agendatypes', 'events'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Agenda->id = $id;
		if (!$this->Agenda->exists())
		{
			throw new NotFoundException(__('Invalid agenda'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Agenda->save($this->request->data))
			{
				$this->Session->setFlash(__('The agenda has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The agenda could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Agenda->read(null, $id);
		}
		$agendatypes = $this->Agenda->Agendatype->find('list');
		$events = $this->Agenda->Event->find('list');
		$this->set(compact('agendatypes', 'events'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Agenda->id = $id;
		if (!$this->Agenda->exists())
		{
			throw new NotFoundException(__('Invalid agenda'));
		}
		if ($this->Agenda->delete())
		{
			$this->Session->setFlash(__('Agenda deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Agenda was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
