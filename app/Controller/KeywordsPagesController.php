<?php
App::uses('AppController', 'Controller');
/**
 * KeywordsPages Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property KeywordsPage $KeywordsPage
 */
class KeywordsPagesController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->KeywordsPage->recursive = 0;
		$this->set('keywordsPages', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->KeywordsPage->id = $id;
		if (!$this->KeywordsPage->exists())
		{
			throw new NotFoundException(__('Invalid keywords page'));
		}
		$this->set('keywordsPage', $this->KeywordsPage->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->KeywordsPage->create();
			if ($this->KeywordsPage->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords page could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->KeywordsPage->Keyword->find('list');
		$pages = $this->KeywordsPage->Page->find('list');
		$this->set(compact('keywords', 'pages'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->KeywordsPage->id = $id;
		if (!$this->KeywordsPage->exists())
		{
			throw new NotFoundException(__('Invalid keywords page'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->KeywordsPage->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords page could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->KeywordsPage->read(null, $id);
		}
		$keywords = $this->KeywordsPage->Keyword->find('list');
		$pages = $this->KeywordsPage->Page->find('list');
		$this->set(compact('keywords', 'pages'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->KeywordsPage->id = $id;
		if (!$this->KeywordsPage->exists())
		{
			throw new NotFoundException(__('Invalid keywords page'));
		}
		if ($this->KeywordsPage->delete())
		{
			$this->Session->setFlash(__('Keywords page deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Keywords page was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->KeywordsPage->recursive = 0;
		$this->set('keywordsPages', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->KeywordsPage->id = $id;
		if (!$this->KeywordsPage->exists())
		{
			throw new NotFoundException(__('Invalid keywords page'));
		}
		$this->set('keywordsPage', $this->KeywordsPage->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->KeywordsPage->create();
			if ($this->KeywordsPage->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords page could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->KeywordsPage->Keyword->find('list');
		$pages = $this->KeywordsPage->Page->find('list');
		$this->set(compact('keywords', 'pages'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->KeywordsPage->id = $id;
		if (!$this->KeywordsPage->exists())
		{
			throw new NotFoundException(__('Invalid keywords page'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->KeywordsPage->save($this->request->data))
			{
				$this->Session->setFlash(__('The keywords page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The keywords page could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->KeywordsPage->read(null, $id);
		}
		$keywords = $this->KeywordsPage->Keyword->find('list');
		$pages = $this->KeywordsPage->Page->find('list');
		$this->set(compact('keywords', 'pages'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->KeywordsPage->id = $id;
		if (!$this->KeywordsPage->exists())
		{
			throw new NotFoundException(__('Invalid keywords page'));
		}
		if ($this->KeywordsPage->delete())
		{
			$this->Session->setFlash(__('Keywords page deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Keywords page was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
