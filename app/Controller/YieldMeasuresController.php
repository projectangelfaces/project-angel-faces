<?php
App::uses('AppController', 'Controller');
/**
 * YieldMeasures Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property YieldMeasure $YieldMeasure
 */
class YieldMeasuresController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->YieldMeasure->recursive = 0;
		$this->set('yieldMeasures', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->YieldMeasure->id = $id;
		if (!$this->YieldMeasure->exists())
		{
			throw new NotFoundException(__('Invalid yield measure'));
		}
		$this->set('yieldMeasure', $this->YieldMeasure->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->YieldMeasure->create();
			if ($this->YieldMeasure->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield measure has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield measure could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->YieldMeasure->id = $id;
		if (!$this->YieldMeasure->exists())
		{
			throw new NotFoundException(__('Invalid yield measure'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->YieldMeasure->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield measure has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield measure could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->YieldMeasure->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->YieldMeasure->id = $id;
		if (!$this->YieldMeasure->exists())
		{
			throw new NotFoundException(__('Invalid yield measure'));
		}
		if ($this->YieldMeasure->delete())
		{
			$this->Session->setFlash(__('Yield measure deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Yield measure was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->YieldMeasure->recursive = 0;
		$this->set('yieldMeasures', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->YieldMeasure->id = $id;
		if (!$this->YieldMeasure->exists())
		{
			throw new NotFoundException(__('Invalid yield measure'));
		}
		$this->set('yieldMeasure', $this->YieldMeasure->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->YieldMeasure->create();
			if ($this->YieldMeasure->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield measure has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield measure could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->YieldMeasure->id = $id;
		if (!$this->YieldMeasure->exists())
		{
			throw new NotFoundException(__('Invalid yield measure'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->YieldMeasure->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield measure has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield measure could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->YieldMeasure->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->YieldMeasure->id = $id;
		if (!$this->YieldMeasure->exists())
		{
			throw new NotFoundException(__('Invalid yield measure'));
		}
		if ($this->YieldMeasure->delete())
		{
			$this->Session->setFlash(__('Yield measure deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Yield measure was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
