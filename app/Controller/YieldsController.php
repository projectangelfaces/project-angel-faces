<?php
App::uses('AppController', 'Controller');
/**
 * Yields Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Yield $Yield
 */
class YieldsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Yield->recursive = 0;
		$this->set('yields', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Yield->id = $id;
		if (!$this->Yield->exists())
		{
			throw new NotFoundException(__('Invalid yield'));
		}
		$this->set('yield', $this->Yield->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Yield->create();
			if ($this->Yield->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Yield->Location->find('list');
		$yieldtypes = $this->Yield->Yieldtype->find('list');
		$yieldMeasures = $this->Yield->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Yield->id = $id;
		if (!$this->Yield->exists())
		{
			throw new NotFoundException(__('Invalid yield'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Yield->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Yield->read(null, $id);
		}
		$locations = $this->Yield->Location->find('list');
		$yieldtypes = $this->Yield->Yieldtype->find('list');
		$yieldMeasures = $this->Yield->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Yield->id = $id;
		if (!$this->Yield->exists())
		{
			throw new NotFoundException(__('Invalid yield'));
		}
		if ($this->Yield->delete())
		{
			$this->Session->setFlash(__('Yield deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Yield was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Yield->recursive = 0;
		$this->set('yields', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Yield->id = $id;
		if (!$this->Yield->exists())
		{
			throw new NotFoundException(__('Invalid yield'));
		}
		$this->set('yield', $this->Yield->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Yield->create();
			if ($this->Yield->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Yield->Location->find('list');
		$yieldtypes = $this->Yield->Yieldtype->find('list');
		$yieldMeasures = $this->Yield->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Yield->id = $id;
		if (!$this->Yield->exists())
		{
			throw new NotFoundException(__('Invalid yield'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Yield->save($this->request->data))
			{
				$this->Session->setFlash(__('The yield has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The yield could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Yield->read(null, $id);
		}
		$locations = $this->Yield->Location->find('list');
		$yieldtypes = $this->Yield->Yieldtype->find('list');
		$yieldMeasures = $this->Yield->YieldMeasure->find('list');
		$this->set(compact('locations', 'yieldtypes', 'yieldMeasures'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Yield->id = $id;
		if (!$this->Yield->exists())
		{
			throw new NotFoundException(__('Invalid yield'));
		}
		if ($this->Yield->delete())
		{
			$this->Session->setFlash(__('Yield deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Yield was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
