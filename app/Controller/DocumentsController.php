<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Document $Document
 */
class DocumentsController extends AppController
{

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Document->recursive = 0;
		$this->set('documents', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Document->id = $id;
		if (!$this->Document->exists())
		{
			throw new NotFoundException(__('Invalid document'));
		}
		$this->set('document', $this->Document->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Document->create();
			if ($this->Document->save($this->request->data))
			{
				$this->Session->setFlash(__('The document has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Document->Location->find('list');
		$profiles = $this->Document->Profile->find('list');
		$programs = $this->Document->Program->find('list');
		$projects = $this->Document->Project->find('list');
		$this->set(compact('locations', 'profiles', 'programs', 'projects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Document->id = $id;
		if (!$this->Document->exists())
		{
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Document->save($this->request->data))
			{
				$this->Session->setFlash(__('The document has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Document->read(null, $id);
		}
		$locations = $this->Document->Location->find('list');
		$profiles = $this->Document->Profile->find('list');
		$programs = $this->Document->Program->find('list');
		$projects = $this->Document->Project->find('list');
		$this->set(compact('locations', 'profiles', 'programs', 'projects'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Document->id = $id;
		if (!$this->Document->exists())
		{
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->Document->delete())
		{
			$this->Session->setFlash(__('Document deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Document was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Document->recursive = 0;
		$this->set('documents', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Document->id = $id;
		if (!$this->Document->exists())
		{
			throw new NotFoundException(__('Invalid document'));
		}
		$this->set('document', $this->Document->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Document->create();
			if ($this->Document->save($this->request->data))
			{
				$this->Session->setFlash(__('The document has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		$locations = $this->Document->Location->find('list');
		$profiles = $this->Document->Profile->find('list');
		$programs = $this->Document->Program->find('list');
		$projects = $this->Document->Project->find('list');
		$this->set(compact('locations', 'profiles', 'programs', 'projects'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Document->id = $id;
		if (!$this->Document->exists())
		{
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Document->save($this->request->data))
			{
				$this->Session->setFlash(__('The document has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The document could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Document->read(null, $id);
		}
		$locations = $this->Document->Location->find('list');
		$profiles = $this->Document->Profile->find('list');
		$programs = $this->Document->Program->find('list');
		$projects = $this->Document->Project->find('list');
		$this->set(compact('locations', 'profiles', 'programs', 'projects'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Document->id = $id;
		if (!$this->Document->exists())
		{
			throw new NotFoundException(__('Invalid document'));
		}
		if ($this->Document->delete())
		{
			$this->Session->setFlash(__('Document deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Document was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
