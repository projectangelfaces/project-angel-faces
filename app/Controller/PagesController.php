<?php
App::uses('AppController', 'Controller');
App::uses('Event', 'Model');
App::uses('Post', 'Model');
/**
 * Pages Controller
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Controllers
 * @since         PAF v 1.0
 *
 *
 * @property Page $Page
 * @property Event $Event
 * @property Post $Post
 */
class PagesController extends AppController
{
    public $uses = array('Page','Post', 'Event');
    /**
     * Displays a view
     *
     * @param $slug $Page What page to display
     * @throws NotFoundException
     * @return void
     */
    public function display($slug)
    {
        $this->layout = 'default';

        $page = $this->Page->findBySlug($slug);

        if(empty($page))
        {
            $path = func_get_args();

            $count = count($path);
            if (!$count) {
                $this->redirect('/');
            }
            $page = $subpage = $title_for_layout = null;

            if (!empty($path[0])) {
                $page = $path[0];
            }
            else
            {
                throw new NotFoundException(__('Invalid page'));
            }
            if (!empty($path[1])) {
                $subpage = $path[1];
            }
            if (!empty($path[$count - 1])) {
                $title_for_layout = Inflector::humanize($path[$count - 1]);
            }
            if($title_for_layout == 'Home')
            {
                $events = $this->Event->find(
                    'all',
                    array(
                        'conditions' => array(
                            '`Event`.`event_start` BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 7 DAY)'
                        )
                    )
                );

                $posts = $this->Post->find(
                    'all',
                    array(
                        'limit' => 5,
                        'order' => 'created ASC'
                    )
                );

                $this->set(
                    compact(
                        'page',
                        'title_for_layout',
                        'posts',
                        'events'
                    )
                );
            }
            else
            {
                $this->set(
                    compact(
                        'page',
                        'subpage',
                        'title_for_layout'
                    )
                );
            }
            $this->render(implode('/', $path));
        }
        else
        {
            $title_for_layout = $page['Page']['title'];
            $this->set(compact('page', 'title_for_layout'));
        }
    }

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Page->id = $id;
		if (!$this->Page->exists())
		{
			throw new NotFoundException(__('Invalid page'));
		}
		$this->set('page', $this->Page->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Page->create();
			if ($this->Page->save($this->request->data))
			{
				$this->Session->setFlash(__('The page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->Page->Keyword->find('list');
		$this->set(compact('keywords'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Page->id = $id;
		if (!$this->Page->exists())
		{
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Page->save($this->request->data))
			{
				$this->Session->setFlash(__('The page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Page->read(null, $id);
		}
		$keywords = $this->Page->Keyword->find('list');
		$this->set(compact('keywords'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		if (!$this->Page->exists())
		{
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->Page->delete())
		{
			$this->Session->setFlash(__('Page deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page was not deleted'));
		$this->redirect(array('action' => 'index'));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Page->recursive = 0;
		$this->set('pages', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Page->id = $id;
		if (!$this->Page->exists())
		{
			throw new NotFoundException(__('Invalid page'));
		}
		$this->set('page', $this->Page->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->Page->create();
			if ($this->Page->save($this->request->data))
			{
				$this->Session->setFlash(__('The page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
		$keywords = $this->Page->Keyword->find('list');
		$this->set(compact('keywords'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Page->id = $id;
		if (!$this->Page->exists())
		{
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Page->save($this->request->data))
			{
				$this->Session->setFlash(__('The page has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Page->read(null, $id);
		}
		$keywords = $this->Page->Keyword->find('list');
		$this->set(compact('keywords'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Page->id = $id;
		if (!$this->Page->exists())
		{
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->Page->delete())
		{
			$this->Session->setFlash(__('Page deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Page was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
