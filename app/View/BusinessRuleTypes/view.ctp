<?php
/**
 * View Business Rule Type View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div id="page-content" class="span9">

    <div class="businessRuleTypes view">
        <div class="row-fluid">
            <h2>
                <?php  echo __('Business Rule Type'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'businessRuleTypes','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>            </h2>
        </div>

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($businessRuleType['BusinessRuleType']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Type'); ?></strong></td>
		<td>
			<?php echo h($businessRuleType['BusinessRuleType']['type']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
</div><!-- #page-content .span9 -->