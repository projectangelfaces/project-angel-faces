<?php
/**
 * Edit Inventory View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2><?php echo __('Edit Inventory'); ?></h2>
                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <?php echo $this->Form->create('Inventory', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
                            <fieldset>

                                <div class="control-group">
	<?php echo $this->Form->label('id', 'Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('location_id', 'Location Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('location_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('yieldtype_id', 'Yieldtype Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('yieldtype_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('yield_measure_id', 'Yield Measure Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('yield_measure_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('amount', 'Amount', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('amount', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('note', 'Note', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('note', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

                            </fieldset>
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

