<?php
/**
 * Admin Add Ledger View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2><?php echo __('Admin Add Ledger'); ?></h2>
                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <?php echo $this->Form->create('Ledger', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
                            <fieldset>

                                <div class="control-group">
	<?php echo $this->Form->label('incometype_id', 'Incometype Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('incometype_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('expensetype_id', 'Expensetype Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('expensetype_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('event_id', 'Event Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('event_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('program_id', 'Program Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('program_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('project_id', 'Project Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('project_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('location_id', 'Location Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('location_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('profile_id', 'Profile Id', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('profile_id', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('amount', 'Amount', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('amount', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('name', 'Name', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('name', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('note', 'Note', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('note', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

                            </fieldset>
                            <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

