<?php
/**
 * View Document View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Documents'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'documents','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($document['Document']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Location'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($document['Location']['name'], array('controller' => 'locations', 'action' => 'view', $document['Location']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Profile'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($document['Profile']['id'], array('controller' => 'profiles', 'action' => 'view', $document['Profile']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Program'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($document['Program']['name'], array('controller' => 'programs', 'action' => 'view', $document['Program']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Project'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($document['Project']['name'], array('controller' => 'projects', 'action' => 'view', $document['Project']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($document['Document']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('File'); ?></strong></td>
		<td>
			<?php echo h($document['Document']['file']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($document['Document']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($document['Document']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
