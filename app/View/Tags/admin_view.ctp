<?php
/**
 * Admin View Tag View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Tags'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'tags','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($tag['Tag']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($tag['Tag']['name']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
        <div class="related">

            <h3><?php echo __('Related Posts'); ?></h3>

            <?php if (!empty($tag['Post'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Content'); ?></th>
		<th><?php echo __('Slug'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($tag['Post'] as $post): ?>
		<tr>
			<td><?php echo $post['id']; ?></td>
			<td><?php echo $post['title']; ?></td>
			<td><?php echo $post['description']; ?></td>
			<td><?php echo $post['content']; ?></td>
			<td><?php echo $post['slug']; ?></td>
			<td><?php echo $post['created']; ?></td>
			<td><?php echo $post['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'posts', 'action' => 'view', $post['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'posts', 'action' => 'edit', $post['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'posts', 'action' => 'delete', $post['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $post['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Post'), array('controller' => 'posts', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
