<?php
/**
 * @var $this View
 */
?>
<!-- Contact form -->
<h4 class="title">Contact Form</h4>
<div class="form">
    <!-- Contact form (not working)-->
    <?php echo $this->Form->create('Contact', array('action' => 'send', 'inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
        <!-- Name -->
        <div class="control-group">
            <label class="control-label" for="ContactName">Name</label>
            <div class="controls">
                <input type="text" class="input-medium" id="ContactName" name="data[Contact][name]">
            </div>
        </div>
        <!-- Email -->
        <div class="control-group">
            <label class="control-label" for="ContactEmail">Email</label>
            <div class="controls">
                <input type="text" class="input-medium" id="ContactEmail" name="data[Contact][email]">
            </div>
        </div>
        <!-- Comment -->
        <div class="control-group">
            <label class="control-label" for="ContactComment">Comment</label>
            <div class="controls">
                <textarea class="input-medium" id="ContactComment" name="data[Contact][comment]" rows="3"></textarea>
            </div>
        </div>
        <!-- Buttons -->
        <div class="form-actions">
            <!-- Buttons -->
            <button type="submit" class="btn">Submit</button>
        </div>
    </form>
</div>
