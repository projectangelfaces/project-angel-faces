<div class="sidebar">
    <div class="sidebar-dropdown">
        <a href="#">Navigation</a>
    </div>
    <!--- Sidebar navigation -->
    <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
    <ul id="nav">
        <!-- Main menu with font awesome icon -->
        <li><a href="index.html" class="open"><i class="icon-home"></i> Dashboard</a>
            <!-- Sub menu markup
                  <ul>
                    <li><a href="#">Submenu #1</a></li>
                    <li><a href="#">Submenu #2</a></li>
                    <li><a href="#">Submenu #3</a></li>
                  </ul>-->
        </li>
        <li class="has_sub">
            <a href="#">
                <i class="icon-money"></i> Accounting
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
                <li><?php echo $this->Html->link('Expense Types', array('controller' => 'expensetypes', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Fees', array('controller' => 'fees', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Income Types', array('controller' => 'incometypes', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Inventories', array('controller' => 'inventories', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Ledgers', array('controller' => 'ledgers', 'action' => 'index')); ?></li>
            </ul>
        </li>
        <li class="has_sub">
            <a href="#">
                <i class="icon-bullhorn"></i> CMS
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
                <li><?php echo $this->Html->link('Pages', array('controller' => 'pages', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Posts', array('controller' => 'posts', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Keywords', array('controller' => 'keywords', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Categories', array('controller' => 'categories', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Comments', array('controller' => 'comments', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Keywords', array('controller' => 'keywords', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Tags', array('controller' => 'tags', 'action' => 'index')); ?></li>
            </ul>
        </li>
        <li class="has_sub">
            <a href="#">
                <i class="icon-calendar"></i> Events
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
                <li><?php echo $this->Html->link('Events', array('controller' => 'events', 'action' => 'index')); ?></li>

            </ul>
        </li>
        <li class="has_sub">
            <a href="#">
                <i class="icon-group"></i> Personnel
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
                <li><?php echo $this->Html->link('Groups', array('controller' => 'groups', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Phones', array('controller' => 'phones', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Profiles', array('controller' => 'profiles', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Tasks', array('controller' => 'tasks', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Teams', array('controller' => 'teams', 'action' => 'index')); ?></li>
        </li>
            </ul>
            
        <li class="has_sub">
            <a href="#">
                <i class="icon-certificate"></i> Programs
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
                <li><?php echo $this->Html->link('Programs', array('controller' => 'programs', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Compost', array('controller' => 'compost', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Schedules', array('controller' => 'schedules', 'action' => 'index')); ?></li>
            </ul>
        </li>
        <li class="has_sub">
            <a href="#">
                <i class="icon-truck"></i> Projects
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
            <li><?php echo $this->Html->link('Projects', array('controller' => 'projects', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Documents', array('controller' => 'documents', 'action' => 'index')); ?></li>
            </ul>
        </li>
        <li class="has_sub">
            <a href="#">
                <i class="icon-map-marker"></i> Properties
                <span class="pull-right">
                    <i class="icon-chevron-right"></i>
                </span>
            </a>
            <ul>
                <li><?php echo $this->Html->link('Addresses', array('controller' => 'addresses', 'action' => 'index')); ?></li>
                <li><?php echo $this->Html->link('Locations', array('controller' => 'categories', 'locations' => 'index')); ?></li>
            </ul>
        </li>
    </ul>
</div>