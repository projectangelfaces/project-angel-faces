<!-- Footer starts -->
<footer>
    <div class="container" id="footer">
        <div class="row">
            <div class="span4">
                <div class="widget">
                    <h5>Sitemap</h5>
                    <hr/>
                    <div class="two-col">
                        <div class="col-left">
                            <ul>
                                

                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'Home',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'home'
                                                        )
                                                    );
                                                ?>

                                            </li>
                                             

                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'About Us',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'about-us'
                                                        )
                                                    );
                                                ?>                                   
                                            </li>
                                            
                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'About Our Founder',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'about-our-founder'
                                                        )
                                                    );
                                                ?>                                   
                                            </li>

                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'About Our Inspiration',
                                                        array(
                                                            'controller' => 'pages',
                                                            'action'    => 'display',
                                                            'our-inspiration'
                                                        )
                                                    );
                                                ?>                                   
                                            </li>

                                        


                                                  <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Neighborhood Fruit Harvest',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'neighborhood-fruit-harvest'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Sustainability Services',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'sustainability-services'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        echo $this->Html->link(
                                                            'Community Support Programs',
                                                            array(
                                                                'controller' => 'pages',
                                                                'action'    => 'display',
                                                                'community-support-programs'
                                                            )
                                                        );
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Urban Tribal Gardens',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'urban-tribal-gardens'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                

                                            
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Get Involved ',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'get-involved'
                                                                )
                                                            );
                                                        ?>
                                                    </li>

                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'Wish List',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'wish-list'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                                    </ul>
                                                    </div>

<div class="col-left">
<ul>
                                                    <li>
                                                        <?php
                                                            echo $this->Html->link(
                                                                'I want to Donate',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'donate'
                                                                )
                                                            );
                                                        ?>
                                                    </li>
                                               
                                                        <li>                                                  
                                                

                                                <?php
                                                            echo $this->Html->link(
                                                                'Whitney Recreation and Senior Center',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'whitney-recreation-and-senior-center'
                                                                )
                                                            );
                                                        ?>
                                                 </li>       

                                                 <li>
                                                         <?php
                                                            echo $this->Html->link(
                                                                'CSN Cheyenne Campus',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'csn-cheyenne-campus'
                                                                )
                                                            );
                                                        ?> 
                                                 </li>

                                                 <li>
                                                         <?php
                                                            echo $this->Html->link(
                                                                'UNLV Campus Community',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'unlv-campus-community'
                                                                )
                                                            );
                                                        ?> 
                                                 </li>

                                                 <li>
                                                         <?php
                                                            echo $this->Html->link(
                                                                'Old Henderson at Jesus Triumphant Christian Complex',
                                                                array(
                                                                    'controller' => 'pages',
                                                                    'action'    => 'display',
                                                                    'old-henderson-at-jesus-triumphant-christian-complex'
                                                                )
                                                            );
                                                        ?> 
                                                 </li>


                                                
                                                    
                                                    <li>
                                                        <?php
                                                            if(!$authUser)
                                                            {
                                                                echo $this->Html->link(
                                                                    'Login',
                                                                    array(
                                                                        'controller' => 'users',
                                                                        'action'    => 'login'
                                                                    )
                                                                );
                                                            }
                                                            else
                                                            {
                                                                echo $this->Html->link(
                                                                    'Logout',
                                                                    array(
                                                                        'controller' => 'users',
                                                                        'action'    => 'logout'
                                                                    )
                                                                );

                                                            }
                                                        ?>
                                                    </li>
                                                    
                                               
                                            <li>
                                                <?php
                                                    echo $this->Html->link(
                                                        'Blog',
                                                        array(
                                                            'controller' => 'posts',
                                                            'action'    => 'listindex'
                                                        )
                                                    );
                                                ?>
                                            </li>
                                            
                                            

                            </ul>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="widget">
                    <h5>Contact</h5>
                    <hr/>
                    <p>Rhonda K. Killough</p>
                    <hr/>
                    <i class="icon-home"></i> &nbsp; 316 Bradford Drive, Henderson, NV 89074<hr/>
                    <i class="icon-phone"></i> &nbsp; (702) 460-9261 <hr/>
                    <i class="icon-envelope-alt"></i> &nbsp; <a href="mailto:planetrhonda@gmail.com">planetrhonda@gmail.com</a>
                    <hr/>
                    <div class="social">
                            <a><?php echo $this->Html->link('<i class="icon-facebook facebook"></i>', 'http://www.facebook.com/ProjectAngelFaces', array('target'=>'_blank', 'escape' => false));?></a>
                            <a><?php echo $this->Html->link('<i class="icon-twitter twitter"></i>', 'http://twitter.com/ShareAngelFaces', array('target'=>'_blank', 'escape' => false));?></a>

                            <a><?php echo $this->Html->link('<i class="icon-linkedin linkedin"></i>', 'http://www.linkedin.com/profile/view?id=23097757', array('target'=>'_blank', 'escape' => false));?></a>
                            
                            <a><?php echo $this->Html->link('<i class="icon-google-plus google-plus"></i>', 'http://plus.google.com/104009000826730647441', array('target'=>'_blank', 'escape' => false));?></a>

                    </div>
                </div>
            </div>
            <div class="span4">
                <?php echo $this->element('contact_send'); ?>
            </div>
            <hr/>
            <!-- Copyright info -->
           
        </div>
        <div class="clearfix">
        </div>
    </div>
    <?php
    echo $this->Html->link(
        $this->Html->image('pi-symbol-animated_20x20.gif', array('width' => '10px')),
        'http://paf-team.t73.biz',
        array(
            'escape' => false,
            'title' => 'Learn more about the GiveCamp PAF Team!'
        )
    )

    ?>
</footer>