<?php
$controllers = array(
'addresses',
'addresses_profiles',
'addresstypes',
'agendas',
'agendatypes',
'areas',
'categories',
'comments',
'composts',
'documents',
'events',
'expensetypes',
'fees',
'functiontypes',
'incometypes',
'inventories',
'keywords',
'keywords_pages',
'keywords_posts',
'ledgers',
'locations',
'pages',
'phones',
'phones_profiles',
'posts',
'posts_tags',
'profiles',
'profiletypes',
'programs',
'projects',
'projecttypes',
'schedules',
'scheduletypes',
'tags',
'tasks',
'tasktypes',
'yield_measures',
'yields',
'yieldtypes'
);

if ($authUser) {
    ?>
    <div class="well sidebar-nav">
        <ul class="nav nav-list">
            <li class="nav-header">Actions</li>
            <?php
            foreach ($controllers as $controller) {
                echo $this->Html->tag(
                    'li',
                    $this->Html->link(
                        Inflector::humanize($controller),
                        array(
                            'controller' => $controller,
                            'action' => 'index',
                            'plugin' => null,
                            'admin' => true
                        )
                    )

                );
            }
            ?>
        </ul>
    </div><!--/.well -->
<?php
}
?>