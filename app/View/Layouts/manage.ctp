<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * @var $this View
 */

$cakeDescription = __d('paf_dev', Configure::read('Sandbox.admin.name'));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>
    <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css(
        array(
            'manage/bootstrap',
            'manage/font-awesome',
            'manage/jquery-ui',
            'manage/fullcalendar',
            'manage/prettyPhoto',
            'manage/rateit',
            'manage/bootstrap-datetimepicker.min',
            'manage/jquery.cleditor',
            'manage/uniform.default',
            'manage/bootstrap-toggle-buttons',
            'manage/style',
            'manage/widgets',
            'manage/bootstrap-responsive',

        )

    );
    echo $this->fetch('meta');
    echo $this->fetch('css/manage/');
    ?>
    <!-- HTML5 Support for IE -->
    <!--[if lt IE 9]>
    <?php
        echo $this->Html->script('html5shim');
    ?>
    <![endif]-->
</head>
<body>
<?php
echo $this->element('manage/header');
?>
<!-- Main content starts -->
<div class="content">
    <!-- Sidebar -->
    <?php
        echo $this->element('manage/sidebar');
    ?>
    <!-- Sidebar ends -->
    <div class="mainbar">
    <!-- Page heading -->
    <div class="page-head">
        <h2 class="pull-left"><i class="icon-home"></i> Dashboard</h2>
        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
            <a href="index.html"><i class="icon-home"></i> Home</a>
            <!-- Divider -->
            <span class="divider">/</span>
            <a href="#" class="bread-current">Dashboard</a>
        </div>
        <div class="clearfix">
        </div>
    </div>
    <!-- Page heading ends -->
    <!-- Matter -->
                                <!-- Content goes here -->
                                <?php echo $this->Session->flash(); ?>
                                <?php echo $this->Session->flash('info'); ?>
                                <?php echo $this->fetch('content'); ?>
    <!-- Matter ends -->
</div>
<!-- Mainbar ends -->
<div class="clearfix">
</div>
</div>
<!-- Content ends -->

<?php
echo $this->element('manage/footer');
?>

<?php
echo $this->Html->script(
    array(
        'manage/jquery',
        'manage/bootstrap',
        'manage/jquery-ui-1.9.2.custom.min',
        'manage/fullcalendar.min',
        'manage/jquery.rateit.min',
        'manage/jquery.prettyPhoto',

        'manage/excanvas.min',
        'manage/jquery.flot',
        'manage/jquery.flot.resize',
        'manage/jquery.flot.pie',
        'manage/jquery.flot.stack',

        'manage/jquery.noty',
        'manage/themes/default',
        'manage/layouts/bottom',
        'manage/layouts/topRight',
        'manage/layouts/top',

        'manage/sparklines',
        'manage/js/jquery.cleditor.min',
        'manage/js/bootstrap-datetimepicker.min',
        'manage/jquery.uniform.min',
        'manage/jquery.toggle.buttons',
        'manage/js/filter',
        'manage/js/custom',
        'manage/js/charts',

    )
);
echo $this->fetch('script');
echo $this->Js->writeBuffer();

$this->TinyMCE->editor(array('theme' => 'advanced', 'mode' => 'textareas'));
?>


</body>
</html>
