<?php
/**
 * @var $this View
 */
?>
<!-- Flex Slider starts -->
<div class="container flex-main">
    <div class="row">
        <div class="span12">
            <div class="flex-image flexslider">
                <ul class="slides">
                    <!-- Each slide should be enclosed inside li tag. -->
                    <!-- Slide #1 -->
                    <li>
                        <!-- Image -->
                        <img src="img/photos/l1.jpg" alt=""/>
                        <!-- Caption -->
                        <div class="flex-caption">
                            <!-- Title -->
                            <h3>Neighborhood Fruit Harvest</h3>
                            <!-- Para -->
                            <p>Harvesting services that collect neighborhood-grown fruits and vegetables and distribute the share to at-risk seniors and youth. Through collaborative partnerships with existing social service agencies we increase access to more fresh food across the socio-economic spectrum.
                            </p>
                        </div>
                    </li>
                    <!-- Slide #2 -->
                    <li>
                        <img src="img/photos/l2.jpg" alt=""/>
                        <div class="flex-caption">
                            <h3>Sustainability Services</h3>
                            <p>A simple system created to lower our community's carbon footprint by providing composting services, seed propagation, seedling cultivation, and repurposing materials to reduce waste.
                            </p>
                        </div>
                    </li>
                    <li>
                        <img src="img/photos/l3.jpg" alt=""/>
                        <div class="flex-caption">
                            <h3>Community Support Programs</h3>
                            <p>These programs build personal, professional, and other life-skills that foster healthy and healthful choices to empower our community's most vulnerable and underemployed.
                            </p>
                        </div>
                    </li>
                    <li>
                        <img src="img/photos/l4.jpg" alt=""/>
                        <div class="flex-caption">
                            <h3>Urban Tribal Gardens</h3>
                            <p>Project AngelFaces provides management and consultation services for the installation and maintenance of organic gardens and small scale orchards for homeowners or businesses.
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Flex slider ends -->
<!-- Service style #1 starts -->
<div class="service-one">
    <div class="container">
        <div class="row">
            <div class="span3">
                <!-- Font awesome icon -->
                <i class="icon-envelope"></i>
                <!-- Title -->
                <h5>Neighborhood Fruit Harvest</h5>
                <!-- Para -->
                <p>We collect local, neighborhood-grown fruits and vegetables and distribute the harvest to existing social service providers which care for seniors and youth.
                </p>
                <!-- Button -->
                <div class="button">
                    <?php
                    echo $this->Html->link(
                        'Register Now',
                        array(
                            'controller' => 'pages',
                            'action'    => 'display',
                            'neighborhood-fruit-harvest'
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="span3">
                <i class="icon-comments-alt"></i>
                <h5>Sustainability Services</h5>
                <p>A simple system created to lower our community's carbon footprint by providing composting services, seed propagation, seedling cultivation, and repurposing materials to reduce waste.
                </p>
                <div class="button">
                    <?php
                    echo $this->Html->link(
                        'Learn More',
                        array(
                            'controller' => 'pages',
                            'action'    => 'display',
                            'sustainability-services'
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="span3">
                <i class="icon-thumbs-up"></i>
                <h5>Community Support Programs</h5>
                <p>These programs build personal, professional, and other life-skills that foster healthy and healthful choices to empower our community's most vulnerable and underemployed.
                </p>
                <div class="button">
                    <?php
                    echo $this->Html->link(
                        'Get Involved',
                        array(
                            'controller' => 'pages',
                            'action'    => 'display',
                            'community-support-programs'
                        )
                    );
                    ?>
                </div>
            </div>
            <div class="span3">
                <i class="icon-thumbs-up"></i>
                <h5>Urban Tribal Gardens</h5>
                <p>Project AngelFaces provides management and consultation services for the installation and maintenance of organic gardens and small scale orchards for homeowners or businesses.
                </p>
                <div class="button">
                    <?php
                    echo $this->Html->link(
                        'Learn More',
                        array(
                            'controller' => 'pages',
                            'action'    => 'display',
                            'urban-tribal-gardens'
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
        <hr/>
    </div>
</div>
<!-- Service style #1 ends -->
<!-- Page content starts -->
<div class="content events">
    <div class="container">
        <div class="row">
            <div class="span6">
                <div class="accordion" id="accordion2">
                    <?php
                        foreach($events as $event)
                        {
                    ?>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                <h5><?php echo $event['Event']['event_name']; ?></h5>
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                <!-- Date and location -->
                                <span><i class="icon-calendar"></i> Date : <?php echo $this->Time->niceShort($event['Event']['event_start']); ?> - <i class="icon-home"></i> Location : <?php echo $event['Location']['name']; ?></span>
                                <!-- Para -->
                                <p>
                                    <?php echo $event['Event']['description']; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php

                        }
                    ?>
            </div>
        </div>
            <div class="span6">
                <div class="widget">
                    <h4>Recent Posts</h4>
                    <ul>
                        <?php
                        foreach($posts as $post)
                        {
                            echo "<li>";
                            echo $this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'show', $post['Post']['slug']));
                            echo "</li>\n";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Page content ends -->