<?php
/**
 * Index Address View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Addresses'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'addresses','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                                <tr>
                                                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('addresstype_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('area_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('address_1'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('address_2'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('address_3'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('city'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('state'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('zip_code'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('note'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                <?php
                                    foreach ($addresses as $address): ?>
	<tr>
		<td><?php echo h($address['Address']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($address['Addresstype']['id'], array('controller' => 'addresstypes', 'action' => 'view', $address['Addresstype']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($address['Area']['name'], array('controller' => 'areas', 'action' => 'view', $address['Area']['id'])); ?>
		</td>
		<td><?php echo h($address['Address']['address_1']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['address_2']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['address_3']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['city']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['state']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['zip_code']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['note']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['created']); ?>&nbsp;</td>
		<td><?php echo h($address['Address']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $address['Address']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $address['Address']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $address['Address']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $address['Address']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                            </table>

                            <p>
                                <small>
                                    <?php
                                    echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>                                </small>
                            </p>

                            <div class="pagination">
                                <ul>
                                    <?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
                                </ul>
                            </div>
                            <!-- .pagination -->

                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
