<?php
/**
 * Admin Add Business Rule View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div id="page-content" class="span9">

    <div class="businessRules form">

        <?php echo $this->Form->create('BusinessRule', array('inputDefaults' => array('label' => false), 'class' => 'form form-horizontal')); ?>
        <fieldset>
            <h2><?php echo __('Admin Add Business Rule'); ?></h2>
            <div class="control-group">
	<?php echo $this->Form->label('rule', 'Rule', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('rule', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('module', 'Module', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('module', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('note', 'Note', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('note', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('created_dt', 'Created Dt', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('created_dt', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

<div class="control-group">
	<?php echo $this->Form->label('removed_dt', 'Removed Dt', array('class' => 'control-label'));?>
	<div class="controls">
		<?php echo $this->Form->input('removed_dt', array('class' => 'span12')); ?>
	</div><!-- .controls -->
</div><!-- .control-group -->

        </fieldset>
        <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>

    </div>

</div><!-- #page-content .span9 -->

