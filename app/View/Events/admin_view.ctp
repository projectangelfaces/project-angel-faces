<?php
/**
 * Admin View Event View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?>
<div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Events'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'events','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

        <table class="table table-striped table-bordered">
            <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($event['Event']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Location'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($event['Location']['name'], array('controller' => 'locations', 'action' => 'view', $event['Location']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Program'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($event['Program']['name'], array('controller' => 'programs', 'action' => 'view', $event['Program']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Project'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($event['Project']['name'], array('controller' => 'projects', 'action' => 'view', $event['Project']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Event Name'); ?></strong></td>
		<td>
			<?php echo h($event['Event']['event_name']); ?>
			&nbsp;
		</td>
</tr>
            <tr>
                <td><strong><?php echo __('Event Description'); ?></strong></td>
                <td>
                    <?php echo h($event['Event']['description']); ?>
                    &nbsp;
                </td>
            </tr>
            <tr>		<td><strong><?php echo __('Event Start'); ?></strong></td>
		<td>
			<?php echo h($event['Event']['event_start']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Event End'); ?></strong></td>
		<td>
			<?php echo h($event['Event']['event_end']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($event['Event']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($event['Event']['modified']); ?>
			&nbsp;
		</td>
</tr>        </table>
        <!-- .table table-striped table-bordered -->

    </div>
    <!-- .view -->

    
        <div class="related">

            <h3><?php echo __('Related Agendas'); ?></h3>

            <?php if (!empty($event['Agenda'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Agendatype Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Content'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($event['Agenda'] as $agenda): ?>
		<tr>
			<td><?php echo $agenda['id']; ?></td>
			<td><?php echo $agenda['agendatype_id']; ?></td>
			<td><?php echo $agenda['event_id']; ?></td>
			<td><?php echo $agenda['name']; ?></td>
			<td><?php echo $agenda['content']; ?></td>
			<td><?php echo $agenda['created']; ?></td>
			<td><?php echo $agenda['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'agendas', 'action' => 'view', $agenda['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'agendas', 'action' => 'edit', $agenda['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'agendas', 'action' => 'delete', $agenda['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $agenda['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Agenda'), array('controller' => 'agendas', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Ledgers'); ?></h3>

            <?php if (!empty($event['Ledger'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Incometype Id'); ?></th>
		<th><?php echo __('Expensetype Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Program Id'); ?></th>
		<th><?php echo __('Project Id'); ?></th>
		<th><?php echo __('Location Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($event['Ledger'] as $ledger): ?>
		<tr>
			<td><?php echo $ledger['id']; ?></td>
			<td><?php echo $ledger['incometype_id']; ?></td>
			<td><?php echo $ledger['expensetype_id']; ?></td>
			<td><?php echo $ledger['event_id']; ?></td>
			<td><?php echo $ledger['program_id']; ?></td>
			<td><?php echo $ledger['project_id']; ?></td>
			<td><?php echo $ledger['location_id']; ?></td>
			<td><?php echo $ledger['profile_id']; ?></td>
			<td><?php echo $ledger['amount']; ?></td>
			<td><?php echo $ledger['name']; ?></td>
			<td><?php echo $ledger['note']; ?></td>
			<td><?php echo $ledger['created']; ?></td>
			<td><?php echo $ledger['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'ledgers', 'action' => 'view', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'ledgers', 'action' => 'edit', $ledger['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ledgers', 'action' => 'delete', $ledger['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $ledger['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Ledger'), array('controller' => 'ledgers', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

    
        <div class="related">

            <h3><?php echo __('Related Schedules'); ?></h3>

            <?php if (!empty($event['Schedule'])): ?>

            <table class="table table-striped table-bordered">
                <tr>
                    		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Event Id'); ?></th>
		<th><?php echo __('Functiontype Id'); ?></th>
		<th><?php echo __('Profile Id'); ?></th>
		<th><?php echo __('Scheduletype Id'); ?></th>
		<th><?php echo __('Start'); ?></th>
		<th><?php echo __('End'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                	<?php
								$i = 0;
								foreach ($event['Schedule'] as $schedule): ?>
		<tr>
			<td><?php echo $schedule['id']; ?></td>
			<td><?php echo $schedule['event_id']; ?></td>
			<td><?php echo $schedule['functiontype_id']; ?></td>
			<td><?php echo $schedule['profile_id']; ?></td>
			<td><?php echo $schedule['scheduletype_id']; ?></td>
			<td><?php echo $schedule['start']; ?></td>
			<td><?php echo $schedule['end']; ?></td>
			<td><?php echo $schedule['created']; ?></td>
			<td><?php echo $schedule['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'schedules', 'action' => 'view', $schedule['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'schedules', 'action' => 'edit', $schedule['id']), array('class' => 'btn btn-mini')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'schedules', 'action' => 'delete', $schedule['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $schedule['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
            </table>
            <!-- .table table-striped table-bordered -->

            <?php endif; ?>


            <div class="actions">
                <?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Schedule'), array('controller' => 'schedules', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>            </div>
            <!-- .actions -->

        </div><!-- .related -->

                        </div>
                    <div class="widget-foot">
                        <!-- Footer goes here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
