<?php
/**
 * Admin Index Schedule View
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PAF : Rapid Development Framework for Project Angel Faces. (http://projectangelfaces.org)
 * Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 *
 *
 * @copyright     Copyright 2013, Project Angel Faces. (http://projectangelfaces.org)
 * @link          http://projectangelfaces.org Project Angel Faces
 * @package       App.Views
 * @since         PAF v 1.0
 *
 *
 */
?><div class="matter">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">
                            <h2>
                                <?php echo __('Schedules'); ?><?php echo $this->Html->link('Add <i class="icon-plus"></i>',array('controller'=> 'schedules','action'    => 'add'),array('escape' => false, 'class' => 'btn btn-primary btn-large offset1'));?>                            </h2>

                        </div>
                        <div class="widget-icons pull-right">
                            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>
                            <a href="#" class="wclose"><i class="icon-remove"></i></a>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">

                            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                                <tr>
                                                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('event_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('functiontype_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('profile_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('scheduletype_id'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('start'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('end'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                                                            <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                <?php
                                    foreach ($schedules as $schedule): ?>
	<tr>
		<td><?php echo h($schedule['Schedule']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($schedule['Event']['id'], array('controller' => 'events', 'action' => 'view', $schedule['Event']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($schedule['Functiontype']['id'], array('controller' => 'functiontypes', 'action' => 'view', $schedule['Functiontype']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($schedule['Profile']['id'], array('controller' => 'profiles', 'action' => 'view', $schedule['Profile']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($schedule['Scheduletype']['name'], array('controller' => 'scheduletypes', 'action' => 'view', $schedule['Scheduletype']['id'])); ?>
		</td>
		<td><?php echo h($schedule['Schedule']['start']); ?>&nbsp;</td>
		<td><?php echo h($schedule['Schedule']['end']); ?>&nbsp;</td>
		<td><?php echo h($schedule['Schedule']['created']); ?>&nbsp;</td>
		<td><?php echo h($schedule['Schedule']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $schedule['Schedule']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $schedule['Schedule']['id']), array('class' => 'btn btn-mini')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $schedule['Schedule']['id']), array('class' => 'btn btn-mini'), __('Are you sure you want to delete # %s?', $schedule['Schedule']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                            </table>

                            <p>
                                <small>
                                    <?php
                                    echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>                                </small>
                            </p>

                            <div class="pagination">
                                <ul>
                                    <?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
                                </ul>
                            </div>
                            <!-- .pagination -->

                        </div>
                        <div class="widget-foot">
                            <!-- Footer goes here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
