<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
/**
 * User Model
 *
 * @property Group $Group
 */
class User extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'username';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
        'group_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            ),
        ),
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
        'Group' => array(
            'className'     => 'Group',
            'foreignKey'    => 'group_id',
            'conditions'    => '',
            'fields'        => '',
            'order'         => ''
        )
    );

    /**
     * beforeSave method
     *
     * @param array $options
     * @return boolean This must return true to validate the save.
     */
	public function beforeSave($options = array()) {
		if (isset($this -> data[$this -> alias]['password'])) {
			$this -> data[$this -> alias]['password'] = AuthComponent::password($this -> data[$this -> alias]['password']);
		}
		return true;
	}

	public $actsAs = array('Acl' => array('type' => 'requester'));

	public function parentNode() {
		if (!$this -> id && empty($this -> data)) {
			return null;
		}
		if (isset($this -> data['User']['group_id'])) {
			$groupId = $this -> data['User']['group_id'];
		} else {
			$groupId = $this -> field('group_id');
		}
		if (!$groupId) {
			return null;
		} else {
			return array('Group' => array('id' => $groupId));
		}
	}

}
